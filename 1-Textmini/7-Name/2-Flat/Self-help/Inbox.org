*** Inbox.org
:PROPERTIES:
:VISIBILITY: children
:END:

**** "self-help" | atomized | kinship | do what's needed

Self-help starts from an atomized perspective, and that is intrinsically depressing.

Humans can have a family, clan, tribe, (how do you call this one?), nation, ethnicity, race, and species. 

That's not even mentioning identities and causes one may have beyond genetic.

"How to make myself happy," may be a difficult question.

"What needs doing around here," is a significantly easier one.

The problem with the latter question is that it quickly leads to w-o-r-k...
