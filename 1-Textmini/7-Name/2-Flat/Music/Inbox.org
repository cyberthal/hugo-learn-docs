*** Inbox.org
:PROPERTIES:
:VISIBILITY: children
:END:

**** ergonomics | work | audio | music | + pleasure, - focus

https://www.fastcompany.com/3032868/how-music-affects-your-productivity

Music harms concentration and is suitable only for supplying motivation during repetitive tasks
