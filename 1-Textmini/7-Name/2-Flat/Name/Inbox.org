*** Inbox.org
:PROPERTIES:
:VISIBILITY: children
:END:

**** A man's true name should never be his government name | my names

A woman begins life with her father's surname, and ends it with her husband's.

In times past, a man needed only one name.  In the era of Woke Capital and Cancel Culture, this is no longer the case.  Now a man needs four names, although they can overlap:

1.  A truth name
2.  A work brand
4.  A family name
3.  A government slave ID

The truth name is the name under which he speaks the truth about this world and the next, as he sees it.  This name belongs to God.

The work brand is something marketable for smooth interface with the economy.  The magic of capitalism renders agreement unnecessary for cooperation.

The government slave ID is the designator by which he is known to the state that claims him. 

The family name is the name given by his father.

If a man is confident that his family will always proudly embrace and support his unfiltered opinions, then he is fortunate indeed.  His family name can be the same as his truth name.  

If a man's family is less tolerant, but his name is bold, memorable and easily spelled (like "Trump"), then his family name can be his work brand.

If his family is unsupportive and his name is unbrandable, he may as well legally change his name to something brandable.

I would suggest, for security by obscurity, and for symbolic reasons, that one never make one's government name the same as one's truth name.  If one intends to use one's family name for one's truth name, then legally change the government name to something else.

The government may punish your body, but it does not own your soul.  Your name should reflect that.

In my case, my true name is Littlebook, my work brand is Cyberthal, and my family name is my government name, since both entities have similar views.

Certainly nobody can object to a name change, after 8 years of Barack Obama / Barry Soetoro, and the current trans rights crusade.  Alias shaming is, like, something that Trump would do.
