*** Inbox.org
:PROPERTIES:
:VISIBILITY: children
:END:

**** civic nationalism | "racial tensions" | civnat dismissal | rebutted

[QUOTE="WJK, post: 889350, member: 51608"]
These racial tensions don't make much sense to me because they don't solve the underlying problems. I have concluded that all this current noise is a smokescreen for a bigger undisclosed agenda.
[/QUOTE]

[BLM Google trends matches 4-year election cycle graphic]
https://qmap.pub/read/4503

"In multiracial societies, you don't vote in accordance with your economic interests and social interests, you vote in accordance with race and religion. Supposing I'd run their system here, Malays would vote for Muslims, Indians would vote for Indians, Chinese would vote for Chinese. I would have a constant clash in my Parliament which cannot be resolved because the Chinese majority would always overrule them."

 - Lee Kuan Yew
https://isteve.blogspot.com/2005/08/lee-kwan-yew-on-democracy-vs.html

"Progress, far from consisting in change, depends on retentiveness. When change is absolute there remains no being to improve and no direction is set for possible improvement: and when experience is not retained, as among savages, infancy is perpetual. Those who cannot remember the past are condemned to repeat it."

- George Santayana
https://en.wikiquote.org/wiki/George_Santayana
[2020-07-13 Mon 02:17]
