*** Inbox.org
:PROPERTIES:
:VISIBILITY: children
:END:

**** text | plain | The Plain Person’s Guide to Plain Text Social Science | Emacs, VC, R

The Plain Person’s Guide to Plain Text Social Science
Kieran Healy
This version: 2019-10-04.
Duke University
kjhealy@soc.duke.edu
http://plain-text.co/index.html#introduction

Very good guide to Emacs, VC and data analysis with R.
[2019-12-26 Thu 22:50]

**** Textmind | Emacs | distros | non-Spacemacs | won't support

I won't even try to support non-Spacemacs distros, except that I create the MELPA packages.
