*** Inbox.org
:PROPERTIES:
:VISIBILITY: children
:END:

**** curated lists of public exominds

#+begin_quote
Kasper ZuttermanToday at 3:14 PM

As of now I curate a list of public second brains, just read only though: https://github.com/KasperZutterman/Second-Brain
#+end_quote

another list:
https://beepb00p.xyz/exobrain/

**** exomind | health | signs | peace of mind and creative output

dkazand> There are two signs that your Second Brain is working well: peace of mind and creative output
