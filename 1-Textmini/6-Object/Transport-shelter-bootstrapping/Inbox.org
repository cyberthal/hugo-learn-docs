*** Inbox.org
:PROPERTIES:
:VISIBILITY: children
:END:

**** mobile shelter bootstrapping

#+begin_quote
ShadoHandToday at 4:40 PM
Camper Trailer Fifth Wheel is still conspicuous.  Those things require hook ups at RV parks and those get pricey after a few nights.  A van can be rotated on a nightly basis a 5th Wheel cannot.  A lot of camp sites are open at night.  All one has to do is park inside after dark and leave before they open in the morning and you can camp for free.  Boat launches and fishing areas are good places to park too.  As are rest areas on high ways.  Rest areas are patrolled by State Police and its where they do Truck Inspections sometimes.  The cops wont know you're there if you're gone early enough in the morning.
[4:42 PM]
Another good place to park is apartment parking in mid size to large towns.  These a lot of time utilize parking space behind buildings and have grassy areas with trees.  If one gets a good enough spot they can conceal the windows with the trees.  A passing car wont be able do look in.
[4:45 PM]
A lot of these parking lots are centrally located too so you can easily travel by foot or bike to a location close by to work.  They also have traffic, whether vehicle or pedestsian, so a van parked there will be hiding in plain sight.  If one develops a routine and can get similar spots each day it wont look suspicious to the normal traffic passing by.
[4:46 PM]
Ive litterally watched the police through a sideview mirror write reports at shift change from the darkness of a reclined passanger seat.
#+end_quote

Obviously if you can't afford to leave the trailer somewhere appropriate, then you can't afford a trailer.
[2020-06-11 Thu 17:08]

**** add rolling suitcase to the mobility bootstrapping

**** Use a scooter convertible into a 2h warclub.

**** add electric monowheel to my boostrapping mobility guide

**** Why van living is bad

Why van living is bad:
https://aidanmaclear.wordpress.com/2020/05/31/blaq-violence-aidans-guide-to-da-streetz/#comment-2010

**** car life | works | Akuma@

#+begin_quote
ShadoHandToday at 11:34 AM

I made more money sleeping in a car and traveling around for work.  1000 a week. ...
Its really safe if you know how to scout an area.  Theres safe and legal 24 hour parking all over the place.  One just has to be able to do some recon.  Plus the Cops dont care once they found out you have a job and are in town only until your contract is up.  Ive been woken up at least a dozen times by cops banging on my window, and they don't give a flying fuck once they found out what I do for work.
#+end_quote

Car is one of the stages in the OP, because pickups cost more.
[2020-06-06 Sat 11:45]

**** bootstrapping | transport and shelter
 
Vehicles are vulnerable to infantry due to blind spots and restricted egress. A pickup has fewer blind spots than a van.

The camper trailer can be left at a park, while the pickup truck commutes, with the option to camp in the bed or off the hitch/tailgate.

A shipping container home is a silly maladapted gimmick with no commercial value, dwarfed by a gigantic industry serving the real need -- mobile/manufactured homes.

**** bootstrapping cont

https://www.reddit.com/r/vandwellers/comments/33jpv5/van_life_vs_rv_life/

#+begin_quote
level 1
[deleted]
2 points
5 years ago

IMO RVing is something wealthy or retired people do. You need a steady income to pay for hook ups and spots. Vandwelling is more stealth and much cheaper because you find free parking and live off city amenities.

level 2
ruat_caelum
2 points
5 years ago

I think a better way of wording it is saying the van dwelling allows you the option to live illegally if you so choose. (You can't really hide a camper easily where as you can a van.) But there are a lot of people that camp on BLM or national land / walmart / campgrounds, and pay to stay, or stay legally for free.
And its only cheaper if you aren't ticketed or towed.
#+end_quote
[2020-06-07 Sun 03:58]

The only thing vanlife is actually good at is stealthy illegal urban camping. But that's a bad goal. While away from the van, it can be ticketed or towed, creating serious problems for someone without an address or financial reserves. The homeless are favorite targets of urban criminals.

Megacities have public transportation, are expensive, and have competitive labor markets. Everything is dense. It's better to live in a small apartment, and use light transport that can travel through public transportation. One can reach a large economic region that way.

The benefit of nomadism is that one can access a large pool of employers outside megacities. Those employers have less competitive labor markets because most employees face high relocation costs, unlike you. This competitive advantage vanishes in megacities.
[2020-06-07 Sun 04:21]

**** bootstrapping cont

#+begin_quote
ShadoHandToday at 4:28 AM

stealthy illegal urban camping. 
@Cyberthal This is a myth.  ecode360 has the parking codes for every city and town in most cases.  Perusing these can help one find loopholes of where to legally stealthily camp.  Also most cities and towns do not enforce parking regulations in certain municipal parking lots because of lack of parking spaces for everyone.  In some of these I've seen people leave boat trailers for weeks.  The only time one runs into problems is in winter due to plows.  Even then one can get up at the right time to avoid the plows.
[4:30 AM]
A lot of places that you don't think would, actually have 24 hour parking.  As long as one rotates their vehicle between these spaces, there is nothing the cops can do, and most parking tickets are only 25 dollars and do not effect ones driving record.
Most van lifers are lazy though and cannot be expected to understand counterintelligence.  If one wants to remain super hidden and disrupt pattern recognition it is imperative to camouflage ones van with a business name.
#+end_quote
[2020-06-07 Sun 04:34]

cheap RV

https://www.curbed.com/2018/8/31/17801610/best-teardrop-trailer-for-sale-camper-rv-buy
Car-towable teardrop trailers start at 10k USD

https://www.twowanderingsoles.com/blog/diy-campervan-conversion-on-a-tiny-budget-in-less-than-1-week
3k USD DIY campervan

But tools needed, and workshop.

Used conversion vans can be ridiculously cheap. I guess it does make sense to replace the car stage with a camper van, likely DIY. A car and teardrop trailer aren't as good. No garage means no reason to prefer the small car size.

Campervan is still an urban solution. It leans on big city amenities for comfort. The pickup and trailer stage ends that dependence.

So campervan gives easy access to all the megacities, whereas pickup+trailer gives access to the rest of the country. Keeping both would be a hassle, but campervans can be rented for comfortable megacity scouting.
[2020-06-07 Sun 05:06]

#+begin_quote
ShadoHandToday at 4:42 AM
One thing I have yet to see anyone do is get a permit for a 24 hour parking garage and park their van in a dark corner and sleep in it.
[4:43 AM]
Ive scouted a couple of parking garages in downtown areas of a couple of cities.  Many businesses (i.e. restaurants mostly) parking their delivery vehicles in areas like these when they are not in use.  These vans do not get burglarized as criminals know there is nothing of value in them.
#+end_quote
[2020-06-07 Sun 05:07]

**** vanlife | cities | bicycle trailers | Lafond@Banjo

#+begin_quote
Banjo	June 7, 2020 10:22 PM UTC

I have lived for short periods in two different vehicles. The first was a pickup truck and the second a van. Some cities have been outlawing sleeping in vehicles because of the explosion of people doing it especially on the west coast. I recommend a windowless cargo van. A sprinter van is ideal due to its high roof. Pickups can be conspicuous because if one is to sleep in the bed he has to exit the vehicle and climb into the back which is an odd thing to do for most people thus raising suspicion. With a van one can wait till the coast is clear and hop into the back. Additionally a pickup with a topper has less room and can be entered more easily by thieves. Many commercial vans may come with a locking cage and the ability to lock the doors with a padlock on the outside when one leaves the van for a while. Vans often have ground clearance close to that of pickups so they work if one is going onto backwoods roads. Of course there are 4x4 vans but they can often be cost prohibitive. 

Camper vans are great if you are not staying in a city. Same with mobile homes, converted buses and the like. Using those in the city may cause people to call the police. You see, there are two main choices when sleeping in a vehicle in the city. One is to find the areas where there are signs of other people sleeping in their vehicles. These are safe zones in bad areas of the city or often underneath an interstate bridge or on a frontage road but they come at a price which is that they are often populated by drug addicts and assorted scumbags. The second choice is to have a decent looking vehicle that blends into all the other vehicles. Then one can sleep in almost any neighborhood that isn't gated. Having done both the second choice is the safest and best. 

As far as bicycles go the best bet in my opinion is to get a cargo trailer. I purchased and used a BOB Yak many years ago which comes with a waterproof sack for the items being trailered. If one is going to be living and moving around by bicycle this is better than pannier racks for a few reasons. First, at least with the BOB Yak, one can take the trailer off very easily. Second, the pannier racks can change the balance of the bike. A good cargo trailer isn't really felt and the balance stays the same. Third, one can carry more in a trailer including groceries, game, firewood etc. 

There once upon a time was a great yahoo group titled Vandwellers which had answers to every question one could come up with when it came to living in a van. Yahoo groups has deleted all the content now which is a shame. Here are some alternative sites to learn more: 

https://www.cheaprvliving.com/forums/index.php

https://www.facebook.com/groups/Vandweller/

https://groups.io/g/VanDwellersOriginal
#+end_quote

**** bootstrap transport shelter | optimal | cargo van camper + camper trailer + bike & trailer

Optimal bootstrap transport shelter:
cargo van camper towing camper trailer

maximizes flexibility city and rural, and vehicle still has good cargo capacity.

Pickup should be rented when needed, or delivery purchased.

bike rack on top can store bike and trailer, I reckon.
or would clearance be an issue?
can be stored inside in a pinch tho.
[2020-06-09 Tue 12:14]

**** vanlife | towing

#+begin_quote
Posted By: Capt Skup on 09/15/04 09:39am 

Sounds like you have a pretty good van for towing if it came equipped with the factory tow option. The tow option should include important items such as transmission, power steering coolers, higher output alternator, rear differential gear ratio(3:73, 4:10), higher rated suspension components, etc.. The vans full frame, rwd, and V-8 power are a plus. I am surprised more people choose to tow with Expeditions, Suburbans instead of full size vans. I think the van makes a much better family tow vehicle, much more usable interior room. Do have the van weighed to find out how much weight the conversion added, you will have to subtract the weight of the van from the posted(drivers door jam) GVWR to find out how much weight you can distribute between people, cargo, fuel, and tongue weight of the trailer. Happy
#+end_quote

#+begin_quote
Posted By: TXiceman on 09/15/04 11:31am 

Most conversion vans are pretty heavy to start with. Better get it weighed when loaded for travel with cargo and passengers. The tow raing from Ford is based on a basic van, no options, no cargo and a 150# driver. By adding weight in thes catagories, you will decrease the towing limit.

Once you have a real weight on your truck...get the GCWR from the owners manual for your engine and axle ratio. The door jam has a sticker with the GVWR.

GCWR - loaded truck = Max. loaded trailer weight you can tow.

GVWR - loaded truck = max. hitch weight you can apply to the truck.

Als, the trailer dry weight is a usless number. Add options such as a battery, A/C, microwave, and you camping supplies and food, you can easily add 1000# to the dry weight.

Realistically you need to be looking a trailers in the 4000# max as a dry weight, provided the van has the tow package.

Have fun shopping.

Ken 
#+end_quote

#+begin_quote

Topic: Conversion Van for towing
Posted By: HybridCamper on 09/15/04 09:18am 

Does anyone tow with either a passenger van or a conversion van. From most of the post, it appears that most people use a pickup truck for towing. My trailer GW is less than 5000 # and I have a 1998 Ford E-150 with a 5.4 and the tow package, it is a conversion van, but no high top. The van is rated to tow 6900#. What, if any problems would I have towing with the van. Thanks 

2004 TravelStar 23SRG
1998 Ford E-140 5.4L
1999 Chevy Tahoe 5.7L 3.73
Wife and 2 daughters 
 

Posted By: Capt Skup on 09/15/04 09:39am 

Sounds like you have a pretty good van for towing if it came equipped with the factory tow option. The tow option should include important items such as transmission, power steering coolers, higher output alternator, rear differential gear ratio(3:73, 4:10), higher rated suspension components, etc.. The vans full frame, rwd, and V-8 power are a plus. I am surprised more people choose to tow with Expeditions, Suburbans instead of full size vans. I think the van makes a much better family tow vehicle, much more usable interior room. Do have the van weighed to find out how much weight the conversion added, you will have to subtract the weight of the van from the posted(drivers door jam) GVWR to find out how much weight you can distribute between people, cargo, fuel, and tongue weight of the trailer. Happy Trails. 

Capt Skup
AD-1(AW)USNRet.
Wonderful Wife,3 Daughters,2Goldens Gus&Riley

"Never get in a battle of wits with an unarmed man"

F-450 Lariat 4x4
Honda Odyssey EX-L
SunlineF281SR
SeaRay 240 Sundeck
http://community.webshots.com/user/CaptSkup?vhost=community 
 

Posted By: TXiceman on 09/15/04 11:31am 

Most conversion vans are pretty heavy to start with. Better get it weighed when loaded for travel with cargo and passengers. The tow raing from Ford is based on a basic van, no options, no cargo and a 150# driver. By adding weight in thes catagories, you will decrease the towing limit.

Once you have a real weight on your truck...get the GCWR from the owners manual for your engine and axle ratio. The door jam has a sticker with the GVWR.

GCWR - loaded truck = Max. loaded trailer weight you can tow.

GVWR - loaded truck = max. hitch weight you can apply to the truck.

Als, the trailer dry weight is a usless number. Add options such as a battery, A/C, microwave, and you camping supplies and food, you can easily add 1000# to the dry weight.

Realistically you need to be looking a trailers in the 4000# max as a dry weight, provided the van has the tow package.

Have fun shopping.

Ken 

Amateur Radio Operator.
2013 HitchHiker 38RLRSB Champagne, toted with a 2012, F350, 6.7L PSD, Crewcab, dually. 3.73 axle, Full Time RVer.
Travel with a standard schnauzer and a Timneh African Gray parrot 

Posted By: Koldei on 09/15/04 11:41am 

I tow with my '92 conversion, just haven't towed much weight.

i can't help but think, though why you don't see 3/4 ton conversions? big comphy insides, and ya aint gonna want to tow another toy?


robert 

1972 Rectrans Discoverer25R
Dodge M300 Chassis w/413 
 

Posted By: Capt Skup on 09/15/04 12:29pm 

Robert, what the heck is that thang in your sig? Kind of looks like a shark. Is it a homebuilt MH? 

Posted By: TPaul on 09/16/04 11:06am 

My 96 Dodge B2500 High top with 5.9/3.55 towed my 5000 lb wet weight trailer OK, but I was right at my van GVWR of 6800 lb with me and the four kids in the van. Never went anywhere with any significant hills, just going over an overpass required my full attention. Also never towed in overdrive either. The van handled every situation well, I put a trans cooler on it and also a trans temp gauge, synthetic oil and rear end fluid. Still have the van but now have the vehicle in my signature because I wanted to go anywhere without worrying if my vehicle was going to make it. 

Tom
98 Chevy 3500 Crew Cab, 7.4L, 4.10
23' Cabana Hybrid Trailer 
 

Posted By: RSD559 on 09/16/04 12:10pm 

Our E-150 type van pulled and handled just fine with our trailer. But pulling that weight just wore it out. It started with u-joints and an axle, the rear end had started to whine, and the transmission was showing signs of premature demise. Even though it was safe and within the towing limits, durability was lost. It eventually died between Mojave and Barstow California, yea I know, lovely place in the middle of summer. We lost another u-joint, with little warning. That was the last straw. While waiting for the tow truck, we called back to Mojave looking for a used 3/4 ton crew cab truck, no go, called ahead to Barstow. Souters sent out their own wrecker and a ride for us. They had us over a barrel, but treated us very well. They were generous on the trade of the van, towing it the fifty + miles for free, and were fair on the price of the truck. The sales dude said that people breaking down in the desert and calling for a new car happens all of the time. They only had an F-350 dually in the used trucks, but the new ones weren't that much more expensive, so we got one. I'm not worried about the trailer wearing this tow vehicle out. 

2020 Torque T314 Toy Hauler Travel Trailer- 38' tip to tip.
2015 F-350 6.7L Diesel, SRW.
Suzuki Burgman 400 scooter. 

Posted By: pattyz on 09/15/04 02:21pm 

Hi,
Our van was rated to tow 6800 lbs also, we had trailers always under 5,000 lbs and did fine towing. You have the very best way to travel sitting right in your driveway. We have towed with our 95 Chev Van for seven years and we love it. Its a high top with leather and the works, but we have five dogs who camp with us, they chill out in the back with the big leather chairs, and on the floor. We drive to Mrytle Beach 10 hours and the old lady here lays back the seat and sleeps down I 95. No pickup truck can compare to the comfort of traveling in a conversion van. This is our second one, the first was a 99 red Ford E conversion van, low top, plush everything. Both vans came with factory tow package installed hitch ready. They tow great ,we did not tow in the mountain areas, but we travel mostly to beach areas which is flat land. We wind up taking all our friends with us while camping out to dinner etc. because everyone else tow with pickup trucks, that have no room in them, or option two, you pay big bucks for the 3 or 4 door models. We also have a Ford Excursion 4X4 desiel to tow with. Hope this helps!!
Happy camping
Pat and Jim and all five fur kids 
#+end_quote

#+begin_quote
Posted By: hertfordnc on 03/26/05 08:04pm 

One thing to add to the discussion about vans is the startup cost. Conversion vans are CHEAP. The conversion van market has tanked in recent years and the result is a lot of cheap vans out there. I got a great deal on mine and after I upgrade the rear end and do a few other things I'll still have way less into it than a comparable truck or suburban and I can seat seven people and two dogs and watch movies. I guess I'm carrying around an extra 1700 lb. But it's a nice ri
#+end_quote

https://www.rv.net/forum/index.cfm/fuseaction/thread/tid/14360724/print/true.cfm

Yep, cargo van camper + camper trailer is the way to travel, period.

Gas mileage will hurt, using it as your daily driver. There are ways to mitigate that, though.

**** vanlife | rooftop tent

Rooftop Tent
https://www.reddit.com/r/Damnthatsinteresting/comments/jl9m3x/rooftop_tent/

On a jeep.  Nice idea for vanlife.
[2020-10-31 Sat 18:57]
