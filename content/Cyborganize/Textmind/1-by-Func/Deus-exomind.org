+++
title = "Future history"
chapter = false
+++

** Deus exomind

*** Paper to digital

[[https://en.wikipedia.org/wiki/Getting_Things_Done]["Getting Things Done"]] (GTD) by David Allen is a productivity system implemented in Emacs Org mode, which Textmind adopts with modifications.  [[https://www.reddit.com/r/Zettelkasten/][Zettelkasten]] is an hierarchy of atomic notes in filing cabinets, which is similar to how Textmind stores atomic headings in an infinitely-deep directory hierarchy.

GTD and Zettelkasten were designed for paper.  Textmind is a digital native, and its workflow is impossible to replicate on paper.  Because it was never limited by the obsolete constraints of paper, Textmind can achieve the goals of both systems simultaneously: adaptive execution and comprehensive knowledge management.  The two goals are one: knowledge directs action; action tests knowledge.

The leap from paper to digital would seem to be an easy one, an upgrade that makes everything better.  Experience teaches otherwise.  The vastly increased intake capacity of digital overwhelms obsolete paper methods and inefficient software UIs.  The result is laggy software, choked systems, and abandoned exominds.

The ergonomics of Emacs, the [[https://www.eigenbahn.com/2020/01/12/emacs-is-no-editor][generic Man-Machine Interface for anything text]], makes it physically possible to comprehensively manage all of one's important thoughts, and transform them into intelligent action.  I wrote the Emacs package [[https://treefactor-docs.nfshost.com][Treefactor]] to make this potential a reality.

*** Human thought algorithms

This leaves the problem of how to abstractly organize the text so that it can continuously sync with a human mind.  The key innovation here is the ramblog processing loop.  It is part of a general cognitive algorithm that will persist across intelligence augmentation technologies, as humans meld with machine.  

That may sound arrogant, but it's merely an observation that effective human thought methods aren't numerous.  That's why aspects of Zettelkasten and GTD have persisted from paper to digital.

(I don't pretend to understand the possibility space for effective *non-human* thought methods.  Ask an AI expert.  I suspect it's large and strange.)

*** Hybrid vigor

The digital medium demands an info manager that won't choke on the volume.  Textmind can sort as much info as one can read, without ever lagging.  It is an exomind that lasts a lifetime.

Textmind realizes the dream of a true exomind, achieving sync latency low enough to extend proprioception into the machine, making one a cyborg, albeit one skingapped by keyboard and screen. 

In fact, I predict that the first practical "thinking" brain-computer interface will merely provide faster typing for the disabled.  What else would it do?

Currently the best mind-machine interface is a Microsoft Natural Ergonomic keyboard and a large screen or two, connected to a modern desktop computer with a Nix-like OS and a fast Internet connection, running Emacs. That combination may sound surprising, but nothing else offers comparable bidirectional bandwidth and latency between brain and bits.

This sync speed has a remarkable result. Mammalian brains are plastic. Proprioception is a matter of latency and feedback. Add a limb in a mirror, and the mind will begin to accept it as part of the body. Likewise, Textmind can extend cognitive proprioception into the machine, by syncing with the brain. It is thus the first intelligence-augmenting cybernetic system.

There are profound benefits to having a mind that is part machine. The strengths and weaknesses of the brain and the computer are complementary. Textmind combines the two into a mind that has the strengths of both and the vulnerabilities of neither.
