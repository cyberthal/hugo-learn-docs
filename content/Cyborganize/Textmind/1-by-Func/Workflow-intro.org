+++
title = "Workflow Intro"
chapter = false  
weight = 1
+++

** Walkthrough

Textmind is not complicated, but it is different.  This page gives a gentle, non-technical introduction.

*** The Daylog

Textmind's first principle is daylogging.  A log is chronological.  A daylog records the day, in chronological order.  It's the main text inbox.  Entries are separated by timestamps like this:

#+begin_quote
Ate a sandwich \\
while listening to music \\
[2020-01-27 Mon 10:34]

surfed reddit \\
[2020-01-27 Mon 10:35]
#+end_quote

Read an interesting article and want to save it?  Paste it into the daylog.

Why keep everything together in one file?  Because you are one person.  You read that article, and it will affect what you think and do next.  Only a log that keeps everything together can convey that.

A daylog that also contains all text saved during the day is a unique concept, AFAIK.  Most daylogs merely include the events of the day.  So I named Textmind's daylog a "ramblog", a portmanteau of ramble and log.

*** The Meta-Outline

Every morning, take yesterday's daylog and process it into atomic notes.  Atomic means "about one thing".  Then file each note into the meta-outline.

What is the meta-outline?  The directory tree is an outline.  Org files contain outlines.  The combined outline of directory tree and Org outlines is the meta-outline.

The meta-outline holds all of your text thoughts.  It's a whole-mind map, which can and should grow enormous.

*** The "10 Bins" filing system

The meta-outline needs a comprehensive filing system.  For example, [[https://en.wikipedia.org/wiki/Dewey_Decimal_Classification][Dewey Decimal Classification]] is a filing system.  But Dewey Decimal doesn't work for an individual.  For example, it has no code for tasks and projects.

10 Bins is the foundation of Textmind.  The best way to learn about it is to look at the directory tree and the documentation simultaneously.  To do that, visit the [[https://github.com/cyberthal/10-Bins-template][template]] on Github.

*** The Head-Up Display

**** Library: shelves vs table

The meta-outline is a huge library with a lifetime's worth of notes.  Libraries are great.  The book are all neatly filed on the shelves.  

But when you must write a report, do you go to the shelves and start writing?  Of course not!  You go to a table and spread out your materials.

Imagine how silly it would be if you were simultaneously writing your report and also refiling pieces of it across the shelves.  Way too difficult!

**** The table: Hud

Textmind's 10 Bins hold your library shelves.  ~2-Linked/Hud~ is your table.  

HUD stands for "Head-Up Display".  It's called that because you use your Hud to orient yourself, like a fighter pilot looks at his HUD.  I suppose in this analogy the 10 Bins would be the huge number of instrument readouts below the cockpit canopy.

Anyway, short names are good for top-level directories.  ~2-Linked/Hud~ is the most frequently-accessed directory in Textmind, so it needs a short name.

The HUD uses three files:
1.  '1rambling.org
2.  '2rambled.org
3.  '3dashboard.org

You've already been introduced to the Ramblog ~1rambling.org~.  Its rules are
1. Strict chronological order
2. Entries separated by timestamps. 

A little outline structure helps the Ramblog, but deep trees are usually too much hassle to keep consistent.

You'll quickly find that working with a Ramblog alone is frustrating.  You can't change old entries, except to more accurately reflect the past.  Past mistakes are set in stone.  (Because they might not be mistakes, and if you change them, you lose the true record.)

That's OK, because ~Hud~ also has ~3dashboard.org~.  The Dashboard is the opposite of the Ramblog.  The Dashboard outline is strict to avoid disorganization risking data loss.  Otherwise you can do whatever you want.  It's there to support your work on the Ramblog.

What can the Dashboard do?
1.  Display key reminders and tasks for the day
2.  Hold complex documents until you finish writing them and transfer them to the ramblog
3.  Manage ongoing multi-task projects via outline

The limitation of the Ramblog is that you don't want too many large complex outlines in it, because that makes the file unwieldy.  But it's easy to transfer such outlines elsewhere in Textmind, and merely link to them from the Dashboard.

For example, if you're writing a thank-you note, compose it in the Dashboard, and then move it to the Ramblog once it's emailed.

Now, why is there a Dashboard inside a HUD?  Those are two opposite things!

Sorry, sometimes evolved systems don't make sense.  Let me know when you figure out what the appendix does.

*** Just-in-Time Sorting

**** Avoiding the infinite maintenance trap

Many PIMs suffer from potentially infinite administrative overhead.  Perfect sync between mind and machine is impossible to achieve via keyboard.  One can go on "improving" the exomind forever without getting anything done.

These PIMs rely on the common sense of the user to decide when to stop.  This is bad design, because FOMO on insights creates stress.  Choosing stopping points is an extremely complex decision that encompasses not only the state of the PIM system and user, but also the subject matter!  A PIM system is supposed to reduce complexity, not multiply it.

Cyborganize avoids this flaw by allowing only two reasons for sorting:
1.  Sorting to a shallow depth via a checklist procedure for system maintenance.
2.  On-demand sorting limited in scope to the specific task you aim to complete right now.  

**** When to process the ramblog

Textmind's main maintenance checklist is for ramblog processing.

Eventually the ramblog gets too long.  It grows annoying to locate info inside of it.  Then it's time to process it into the 10 Bins.

It's usually best to process the yesterday's ramblog in the morning, because you've slept on it, but haven't forgotten it yet.  However, it's also perfectly fine to wait two days, or process twice per day.  

Do whatever's comfortable.  At worst you create a little extra work for yourself.

**** How to process a ramblog

Processing the ramblog is an easy checklist procedure. 

First make a backup of the ramblog for your journal archives!  This preserves your history.

Then follow these steps.  (Remember, this is just an overview.  Later you'll get a detailed explanation.)

1. Delete everything superfluous from the ramblog. 
2. Add heading dividers wherever appropriate.
3. Title the headings
4. Promote headings.  Emancipate headings with inappropriate parents.  GTD headings leave the file via treefactor-up, on their way to ~/1-Agenda~.
5. File headings into the meta-outline.
6. File the 10 Bins ~Inbox.org~ headings one level deeper.  This exploits your current familiarity with the headings, without bogging you down in any detailed work.  It also catches misfiles.
7.  Sort and prioritize new tasks.

Now you're ready to work again!

**** Resolving lack of direction

Suppose you've done all that, but you still don't know what to do next.  You have a list of prioritized tasks in org-agenda, but none of them feel like the right next step.

Should you break the rule against aimless sorting, to try to get your bearings?  No.

First, ramble about your uncertainty in the ramblog.  You make shake loose the blockage.

Next, set as specific an orientation task as you can in the dashboard.  

Then plan the task, as specifically as possible.

Then start sorting, following the sorting instructions you just gave yourself.

I sometimes do a "grand sort," moving through all the 10 Bins.  But because of the narrow scope dictated by my specific purpose, I can ignore the vast majority of the information as I search for the few bits I need.

This keeps my iterational velocity high, which permits me to shortcut my way through imposing obstacles.

It's amazing how many problems don't need to be solved once you define your requirements narrowly enough.  Textmind makes you a genius.  Geniuses are lazy.

They have to be, to free enough energy to build the monuments they leave behind.

*** Getting Things Done (GTD)

Because Textmind is implemented in Emacs Org-mode, it includes lots of task management and productivity features. These include TODO states, tags, links, checklists, etc.

These features add database functionality to the meta-outline. The most common database feature you'll use is the Org Agenda, which lists your tasks and appointments.

Org is great. It's just missing the Textmind workflow philosophy. A little code (e.g. Treefactor) makes the two compatible.

If you want to know more about Org-mode, see the [[https://orgmode.org][website]].

*** Summary

Textmind syncs a meta-outline to your mind, and guides your actions with a GTD database.  It is suitable as a primary personal knowledge manager and productivity tool.

Unlike most competing systems, it can handle hundreds of megabytes of text without lag.  Thanks to Emacs' strong community and open standards, it is future-proof.  Textmind is the last productivity system you'll ever need.
