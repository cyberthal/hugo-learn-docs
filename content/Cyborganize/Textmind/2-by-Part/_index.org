+++
title = "by-Part"
chapter = false
weight=2
+++

** Content by component

This chapter covers *components* used by Textmind.

For example, linking is a *function* of Textmind.  Org-id is a *component* of Org mode that Textmind uses for linking.  ~1-Agenda~ and ~2-Linked~ are *paths* within the org-id search scope.
