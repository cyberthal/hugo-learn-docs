+++
title = "by Path"
chapter = false  
weight=3
+++

** Template explained

This chapter covers the files and directories in the Textmind [[https://github.com/cyberthal/Textmind-template][template]].  It is organized in the same way as the template's directory tree (in other words, by [[https://en.wikipedia.org/wiki/Path_(computing)][path]]).

It's redundant to document this on the web and also in the template itself.  So please go [[https://github.com/cyberthal/Textmind-template][there]] to read about it.
